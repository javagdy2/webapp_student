package pl.sda.javagdy2.servlets;

import pl.sda.javagdy2.database.EntityDao;
import pl.sda.javagdy2.database.model.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/student/add")
public class StudentAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // w jednej linii tworzę request dispatcher i przekierowuje go na widok formularza (wyświetlam formularz).
        req.getRequestDispatcher("/student_form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Student student = Student.builder()
                .imie(request.getParameter("imie"))
                .nazwisko(request.getParameter("nazwisko"))
                /* jeśli pełnoletni jest zaznaczone, to przyjmuje wartość "on" - text/String/on */
                .pelnoletni(request.getParameter("pelnoletni") != null)
                .wzrost(Double.parseDouble(request.getParameter("wzrost")))
                .build();

        EntityDao dao = new EntityDao();
        dao.saveOrUpdate(student);

        response.sendRedirect(request.getContextPath() +"/student/list");
    }
}
